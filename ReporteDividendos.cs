﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoTBD1
{
    public partial class ReporteDividendos : Form
    {
        public ReporteDividendos()
        {
            InitializeComponent();
        }

        private void ReporteDividendos_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSetProyecto.REPORTE_DIVIDENDOS' table. You can move, or remove it, as needed.
            this.rEPORTE_DIVIDENDOSTableAdapter.Fill(this.dataSetProyecto.REPORTE_DIVIDENDOS);

        }
    }
}
