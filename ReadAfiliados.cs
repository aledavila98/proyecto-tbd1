﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoTBD1
{
    public partial class ReadAfiliados : Form
    {
        public ReadAfiliados()
        {
            InitializeComponent();
        }

        private void aFILIADOSBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.aFILIADOSBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSetProyecto);

        }

        private void ReadAfiliados_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSetProyecto.AFILIADOS' table. You can move, or remove it, as needed.
            this.aFILIADOSTableAdapter.Fill(this.dataSetProyecto.AFILIADOS);

        }
    }
}
