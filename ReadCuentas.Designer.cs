﻿namespace ProyectoTBD1
{
    partial class ReadCuentas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadCuentas));
            this.dataSetProyecto = new ProyectoTBD1.DataSetProyecto();
            this.cUENTASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cUENTASTableAdapter = new ProyectoTBD1.DataSetProyectoTableAdapters.CUENTASTableAdapter();
            this.tableAdapterManager = new ProyectoTBD1.DataSetProyectoTableAdapters.TableAdapterManager();
            this.cUENTASBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.cUENTASBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.cUENTASDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProyecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cUENTASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cUENTASBindingNavigator)).BeginInit();
            this.cUENTASBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cUENTASDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataSetProyecto
            // 
            this.dataSetProyecto.DataSetName = "DataSetProyecto";
            this.dataSetProyecto.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cUENTASBindingSource
            // 
            this.cUENTASBindingSource.DataMember = "CUENTAS";
            this.cUENTASBindingSource.DataSource = this.dataSetProyecto;
            // 
            // cUENTASTableAdapter
            // 
            this.cUENTASTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ABONOSTableAdapter = null;
            this.tableAdapterManager.AFILIADOSTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CUENTASTableAdapter = this.cUENTASTableAdapter;
            this.tableAdapterManager.GANANCIAS_AFILIADOSTableAdapter = null;
            this.tableAdapterManager.GANANCIASTableAdapter = null;
            this.tableAdapterManager.PAGOSTableAdapter = null;
            this.tableAdapterManager.PRESTAMOSTableAdapter = null;
            this.tableAdapterManager.TELEFONOTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ProyectoTBD1.DataSetProyectoTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // cUENTASBindingNavigator
            // 
            this.cUENTASBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.cUENTASBindingNavigator.BindingSource = this.cUENTASBindingSource;
            this.cUENTASBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.cUENTASBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.cUENTASBindingNavigator.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cUENTASBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.cUENTASBindingNavigatorSaveItem});
            this.cUENTASBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.cUENTASBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.cUENTASBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.cUENTASBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.cUENTASBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.cUENTASBindingNavigator.Name = "cUENTASBindingNavigator";
            this.cUENTASBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.cUENTASBindingNavigator.Size = new System.Drawing.Size(871, 27);
            this.cUENTASBindingNavigator.TabIndex = 0;
            this.cUENTASBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(24, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(24, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(45, 20);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // cUENTASBindingNavigatorSaveItem
            // 
            this.cUENTASBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cUENTASBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("cUENTASBindingNavigatorSaveItem.Image")));
            this.cUENTASBindingNavigatorSaveItem.Name = "cUENTASBindingNavigatorSaveItem";
            this.cUENTASBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 23);
            this.cUENTASBindingNavigatorSaveItem.Text = "Save Data";
            this.cUENTASBindingNavigatorSaveItem.Click += new System.EventHandler(this.cUENTASBindingNavigatorSaveItem_Click);
            // 
            // cUENTASDataGridView
            // 
            this.cUENTASDataGridView.AutoGenerateColumns = false;
            this.cUENTASDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cUENTASDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.cUENTASDataGridView.DataSource = this.cUENTASBindingSource;
            this.cUENTASDataGridView.Location = new System.Drawing.Point(67, 71);
            this.cUENTASDataGridView.Name = "cUENTASDataGridView";
            this.cUENTASDataGridView.RowTemplate.Height = 24;
            this.cUENTASDataGridView.Size = new System.Drawing.Size(648, 220);
            this.cUENTASDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TIPO_CUENTA";
            this.dataGridViewTextBoxColumn1.HeaderText = "TIPO_CUENTA";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "NUMERO_CUENTA";
            this.dataGridViewTextBoxColumn2.HeaderText = "NUMERO_CUENTA";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "FECHA_APERTURA";
            this.dataGridViewTextBoxColumn3.HeaderText = "FECHA_APERTURA";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "SALDO";
            this.dataGridViewTextBoxColumn4.HeaderText = "SALDO";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "CODIGO_ABONOS";
            this.dataGridViewTextBoxColumn5.HeaderText = "CODIGO_ABONOS";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "CODIGO";
            this.dataGridViewTextBoxColumn6.HeaderText = "CODIGO";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // ReadCuentas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(871, 418);
            this.Controls.Add(this.cUENTASDataGridView);
            this.Controls.Add(this.cUENTASBindingNavigator);
            this.Name = "ReadCuentas";
            this.Text = "ReadCuentas";
            this.Load += new System.EventHandler(this.ReadCuentas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProyecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cUENTASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cUENTASBindingNavigator)).EndInit();
            this.cUENTASBindingNavigator.ResumeLayout(false);
            this.cUENTASBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cUENTASDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSetProyecto dataSetProyecto;
        private System.Windows.Forms.BindingSource cUENTASBindingSource;
        private DataSetProyectoTableAdapters.CUENTASTableAdapter cUENTASTableAdapter;
        private DataSetProyectoTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator cUENTASBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton cUENTASBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView cUENTASDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    }
}