﻿namespace ProyectoTBD1
{
    partial class ReadTelefono
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadTelefono));
            this.dataSetProyecto = new ProyectoTBD1.DataSetProyecto();
            this.tELEFONOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tELEFONOTableAdapter = new ProyectoTBD1.DataSetProyectoTableAdapters.TELEFONOTableAdapter();
            this.tableAdapterManager = new ProyectoTBD1.DataSetProyectoTableAdapters.TableAdapterManager();
            this.tELEFONOBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.tELEFONOBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.tELEFONODataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProyecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tELEFONOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tELEFONOBindingNavigator)).BeginInit();
            this.tELEFONOBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tELEFONODataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataSetProyecto
            // 
            this.dataSetProyecto.DataSetName = "DataSetProyecto";
            this.dataSetProyecto.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tELEFONOBindingSource
            // 
            this.tELEFONOBindingSource.DataMember = "TELEFONO";
            this.tELEFONOBindingSource.DataSource = this.dataSetProyecto;
            // 
            // tELEFONOTableAdapter
            // 
            this.tELEFONOTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ABONOSTableAdapter = null;
            this.tableAdapterManager.AFILIADOSTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CUENTASTableAdapter = null;
            this.tableAdapterManager.GANANCIAS_AFILIADOSTableAdapter = null;
            this.tableAdapterManager.GANANCIASTableAdapter = null;
            this.tableAdapterManager.PAGOSTableAdapter = null;
            this.tableAdapterManager.PRESTAMOSTableAdapter = null;
            this.tableAdapterManager.TELEFONOTableAdapter = this.tELEFONOTableAdapter;
            this.tableAdapterManager.UpdateOrder = ProyectoTBD1.DataSetProyectoTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // tELEFONOBindingNavigator
            // 
            this.tELEFONOBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.tELEFONOBindingNavigator.BindingSource = this.tELEFONOBindingSource;
            this.tELEFONOBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.tELEFONOBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.tELEFONOBindingNavigator.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.tELEFONOBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.tELEFONOBindingNavigatorSaveItem});
            this.tELEFONOBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.tELEFONOBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.tELEFONOBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.tELEFONOBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.tELEFONOBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.tELEFONOBindingNavigator.Name = "tELEFONOBindingNavigator";
            this.tELEFONOBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.tELEFONOBindingNavigator.Size = new System.Drawing.Size(776, 27);
            this.tELEFONOBindingNavigator.TabIndex = 0;
            this.tELEFONOBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(24, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(24, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(45, 20);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // tELEFONOBindingNavigatorSaveItem
            // 
            this.tELEFONOBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tELEFONOBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("tELEFONOBindingNavigatorSaveItem.Image")));
            this.tELEFONOBindingNavigatorSaveItem.Name = "tELEFONOBindingNavigatorSaveItem";
            this.tELEFONOBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 23);
            this.tELEFONOBindingNavigatorSaveItem.Text = "Save Data";
            this.tELEFONOBindingNavigatorSaveItem.Click += new System.EventHandler(this.tELEFONOBindingNavigatorSaveItem_Click);
            // 
            // tELEFONODataGridView
            // 
            this.tELEFONODataGridView.AutoGenerateColumns = false;
            this.tELEFONODataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tELEFONODataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.tELEFONODataGridView.DataSource = this.tELEFONOBindingSource;
            this.tELEFONODataGridView.Location = new System.Drawing.Point(195, 82);
            this.tELEFONODataGridView.Name = "tELEFONODataGridView";
            this.tELEFONODataGridView.RowTemplate.Height = 24;
            this.tELEFONODataGridView.Size = new System.Drawing.Size(300, 220);
            this.tELEFONODataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TELEFONO";
            this.dataGridViewTextBoxColumn1.HeaderText = "TELEFONO";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CODIGO";
            this.dataGridViewTextBoxColumn2.HeaderText = "CODIGO";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // ReadTelefono
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 397);
            this.Controls.Add(this.tELEFONODataGridView);
            this.Controls.Add(this.tELEFONOBindingNavigator);
            this.Name = "ReadTelefono";
            this.Text = "ReadTelefono";
            this.Load += new System.EventHandler(this.ReadTelefono_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProyecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tELEFONOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tELEFONOBindingNavigator)).EndInit();
            this.tELEFONOBindingNavigator.ResumeLayout(false);
            this.tELEFONOBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tELEFONODataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSetProyecto dataSetProyecto;
        private System.Windows.Forms.BindingSource tELEFONOBindingSource;
        private DataSetProyectoTableAdapters.TELEFONOTableAdapter tELEFONOTableAdapter;
        private DataSetProyectoTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator tELEFONOBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton tELEFONOBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView tELEFONODataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}