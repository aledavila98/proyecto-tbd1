﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoTBD1
{
    public partial class ReadCuentas : Form
    {
        public ReadCuentas()
        {
            InitializeComponent();
        }

        private void cUENTASBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.cUENTASBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSetProyecto);

        }

        private void ReadCuentas_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSetProyecto.CUENTAS' table. You can move, or remove it, as needed.
            this.cUENTASTableAdapter.Fill(this.dataSetProyecto.CUENTAS);

        }
    }
}
