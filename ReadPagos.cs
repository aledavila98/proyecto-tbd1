﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoTBD1
{
    public partial class ReadPagos : Form
    {
        public ReadPagos()
        {
            InitializeComponent();
        }

        private void pAGOSBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.pAGOSBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSetProyecto);

        }

        private void ReadPagos_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSetProyecto.PAGOS' table. You can move, or remove it, as needed.
            this.pAGOSTableAdapter.Fill(this.dataSetProyecto.PAGOS);

        }
    }
}
