﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoTBD1
{
    public partial class ReadTelefono : Form
    {
        public ReadTelefono()
        {
            InitializeComponent();
        }

        private void tELEFONOBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.tELEFONOBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSetProyecto);

        }

        private void ReadTelefono_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSetProyecto.TELEFONO' table. You can move, or remove it, as needed.
            this.tELEFONOTableAdapter.Fill(this.dataSetProyecto.TELEFONO);

        }
    }
}
