﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoTBD1
{
    public partial class ReadAbonos : Form
    {
        public ReadAbonos()
        {
            InitializeComponent();
        }

        private void aBONOSBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.aBONOSBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSetProyecto);

        }

        private void ReadAbonos_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSetProyecto.ABONOS' table. You can move, or remove it, as needed.
            this.aBONOSTableAdapter.Fill(this.dataSetProyecto.ABONOS);

        }
    }
}
