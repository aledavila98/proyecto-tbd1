﻿namespace ProyectoTBD1
{
    partial class ReadGanancias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadGanancias));
            this.dataSetProyecto = new ProyectoTBD1.DataSetProyecto();
            this.gANANCIASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gANANCIASTableAdapter = new ProyectoTBD1.DataSetProyectoTableAdapters.GANANCIASTableAdapter();
            this.tableAdapterManager = new ProyectoTBD1.DataSetProyectoTableAdapters.TableAdapterManager();
            this.gANANCIASBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.gANANCIASBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.gANANCIASDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProyecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gANANCIASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gANANCIASBindingNavigator)).BeginInit();
            this.gANANCIASBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gANANCIASDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataSetProyecto
            // 
            this.dataSetProyecto.DataSetName = "DataSetProyecto";
            this.dataSetProyecto.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gANANCIASBindingSource
            // 
            this.gANANCIASBindingSource.DataMember = "GANANCIAS";
            this.gANANCIASBindingSource.DataSource = this.dataSetProyecto;
            // 
            // gANANCIASTableAdapter
            // 
            this.gANANCIASTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ABONOSTableAdapter = null;
            this.tableAdapterManager.AFILIADOSTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CUENTASTableAdapter = null;
            this.tableAdapterManager.GANANCIAS_AFILIADOSTableAdapter = null;
            this.tableAdapterManager.GANANCIASTableAdapter = this.gANANCIASTableAdapter;
            this.tableAdapterManager.PAGOSTableAdapter = null;
            this.tableAdapterManager.PRESTAMOSTableAdapter = null;
            this.tableAdapterManager.TELEFONOTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ProyectoTBD1.DataSetProyectoTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // gANANCIASBindingNavigator
            // 
            this.gANANCIASBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.gANANCIASBindingNavigator.BindingSource = this.gANANCIASBindingSource;
            this.gANANCIASBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.gANANCIASBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.gANANCIASBindingNavigator.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.gANANCIASBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.gANANCIASBindingNavigatorSaveItem});
            this.gANANCIASBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.gANANCIASBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.gANANCIASBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.gANANCIASBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.gANANCIASBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.gANANCIASBindingNavigator.Name = "gANANCIASBindingNavigator";
            this.gANANCIASBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.gANANCIASBindingNavigator.Size = new System.Drawing.Size(829, 27);
            this.gANANCIASBindingNavigator.TabIndex = 0;
            this.gANANCIASBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(24, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(24, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(45, 20);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // gANANCIASBindingNavigatorSaveItem
            // 
            this.gANANCIASBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.gANANCIASBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("gANANCIASBindingNavigatorSaveItem.Image")));
            this.gANANCIASBindingNavigatorSaveItem.Name = "gANANCIASBindingNavigatorSaveItem";
            this.gANANCIASBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 23);
            this.gANANCIASBindingNavigatorSaveItem.Text = "Save Data";
            this.gANANCIASBindingNavigatorSaveItem.Click += new System.EventHandler(this.gANANCIASBindingNavigatorSaveItem_Click);
            // 
            // gANANCIASDataGridView
            // 
            this.gANANCIASDataGridView.AutoGenerateColumns = false;
            this.gANANCIASDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gANANCIASDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.gANANCIASDataGridView.DataSource = this.gANANCIASBindingSource;
            this.gANANCIASDataGridView.Location = new System.Drawing.Point(254, 86);
            this.gANANCIASDataGridView.Name = "gANANCIASDataGridView";
            this.gANANCIASDataGridView.RowTemplate.Height = 24;
            this.gANANCIASDataGridView.Size = new System.Drawing.Size(261, 220);
            this.gANANCIASDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ID_GANANCIAS";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID_GANANCIAS";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "GANANCIA_TOTAL";
            this.dataGridViewTextBoxColumn2.HeaderText = "GANANCIA_TOTAL";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // ReadGanancias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 446);
            this.Controls.Add(this.gANANCIASDataGridView);
            this.Controls.Add(this.gANANCIASBindingNavigator);
            this.Name = "ReadGanancias";
            this.Text = "ReadGanancias";
            this.Load += new System.EventHandler(this.ReadGanancias_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProyecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gANANCIASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gANANCIASBindingNavigator)).EndInit();
            this.gANANCIASBindingNavigator.ResumeLayout(false);
            this.gANANCIASBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gANANCIASDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSetProyecto dataSetProyecto;
        private System.Windows.Forms.BindingSource gANANCIASBindingSource;
        private DataSetProyectoTableAdapters.GANANCIASTableAdapter gANANCIASTableAdapter;
        private DataSetProyectoTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator gANANCIASBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton gANANCIASBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView gANANCIASDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}