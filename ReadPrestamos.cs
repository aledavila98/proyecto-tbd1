﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoTBD1
{
    public partial class ReadPrestamos : Form
    {
        public ReadPrestamos()
        {
            InitializeComponent();
        }

        private void pRESTAMOSBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.pRESTAMOSBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSetProyecto);

        }

        private void ReadPrestamos_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSetProyecto.PRESTAMOS' table. You can move, or remove it, as needed.
            this.pRESTAMOSTableAdapter.Fill(this.dataSetProyecto.PRESTAMOS);

        }
    }
}
