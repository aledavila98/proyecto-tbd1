﻿namespace ProyectoTBD1
{
    partial class ReadPagos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadPagos));
            this.dataSetProyecto = new ProyectoTBD1.DataSetProyecto();
            this.pAGOSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pAGOSTableAdapter = new ProyectoTBD1.DataSetProyectoTableAdapters.PAGOSTableAdapter();
            this.tableAdapterManager = new ProyectoTBD1.DataSetProyectoTableAdapters.TableAdapterManager();
            this.pAGOSBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.pAGOSBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.pAGOSDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProyecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGOSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGOSBindingNavigator)).BeginInit();
            this.pAGOSBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pAGOSDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataSetProyecto
            // 
            this.dataSetProyecto.DataSetName = "DataSetProyecto";
            this.dataSetProyecto.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pAGOSBindingSource
            // 
            this.pAGOSBindingSource.DataMember = "PAGOS";
            this.pAGOSBindingSource.DataSource = this.dataSetProyecto;
            // 
            // pAGOSTableAdapter
            // 
            this.pAGOSTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ABONOSTableAdapter = null;
            this.tableAdapterManager.AFILIADOSTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CUENTASTableAdapter = null;
            this.tableAdapterManager.GANANCIAS_AFILIADOSTableAdapter = null;
            this.tableAdapterManager.GANANCIASTableAdapter = null;
            this.tableAdapterManager.PAGOSTableAdapter = this.pAGOSTableAdapter;
            this.tableAdapterManager.PRESTAMOSTableAdapter = null;
            this.tableAdapterManager.TELEFONOTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ProyectoTBD1.DataSetProyectoTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // pAGOSBindingNavigator
            // 
            this.pAGOSBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.pAGOSBindingNavigator.BindingSource = this.pAGOSBindingSource;
            this.pAGOSBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.pAGOSBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.pAGOSBindingNavigator.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.pAGOSBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.pAGOSBindingNavigatorSaveItem});
            this.pAGOSBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.pAGOSBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.pAGOSBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.pAGOSBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.pAGOSBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.pAGOSBindingNavigator.Name = "pAGOSBindingNavigator";
            this.pAGOSBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.pAGOSBindingNavigator.Size = new System.Drawing.Size(785, 27);
            this.pAGOSBindingNavigator.TabIndex = 0;
            this.pAGOSBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(24, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(24, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(45, 20);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // pAGOSBindingNavigatorSaveItem
            // 
            this.pAGOSBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pAGOSBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("pAGOSBindingNavigatorSaveItem.Image")));
            this.pAGOSBindingNavigatorSaveItem.Name = "pAGOSBindingNavigatorSaveItem";
            this.pAGOSBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 23);
            this.pAGOSBindingNavigatorSaveItem.Text = "Save Data";
            this.pAGOSBindingNavigatorSaveItem.Click += new System.EventHandler(this.pAGOSBindingNavigatorSaveItem_Click);
            // 
            // pAGOSDataGridView
            // 
            this.pAGOSDataGridView.AutoGenerateColumns = false;
            this.pAGOSDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pAGOSDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.pAGOSDataGridView.DataSource = this.pAGOSBindingSource;
            this.pAGOSDataGridView.Location = new System.Drawing.Point(134, 58);
            this.pAGOSDataGridView.Name = "pAGOSDataGridView";
            this.pAGOSDataGridView.RowTemplate.Height = 24;
            this.pAGOSDataGridView.Size = new System.Drawing.Size(449, 220);
            this.pAGOSDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NUMERO_PAGO";
            this.dataGridViewTextBoxColumn1.HeaderText = "NUMERO_PAGO";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "FECHA";
            this.dataGridViewTextBoxColumn2.HeaderText = "FECHA";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "MONTO";
            this.dataGridViewTextBoxColumn3.HeaderText = "MONTO";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "NUMERO_PRESTAMO";
            this.dataGridViewTextBoxColumn4.HeaderText = "NUMERO_PRESTAMO";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // ReadPagos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 410);
            this.Controls.Add(this.pAGOSDataGridView);
            this.Controls.Add(this.pAGOSBindingNavigator);
            this.Name = "ReadPagos";
            this.Text = "ReadPagos";
            this.Load += new System.EventHandler(this.ReadPagos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProyecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGOSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pAGOSBindingNavigator)).EndInit();
            this.pAGOSBindingNavigator.ResumeLayout(false);
            this.pAGOSBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pAGOSDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSetProyecto dataSetProyecto;
        private System.Windows.Forms.BindingSource pAGOSBindingSource;
        private DataSetProyectoTableAdapters.PAGOSTableAdapter pAGOSTableAdapter;
        private DataSetProyectoTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator pAGOSBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton pAGOSBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView pAGOSDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    }
}