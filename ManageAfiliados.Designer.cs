﻿namespace ProyectoTBD1
{
    partial class ManageAfiliados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbCodigo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbSegundoNombre = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbPrimerApellido = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbEmailPrimario = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbEmailSecundario = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbNumeroPrestamo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbNumeroCuenta = new System.Windows.Forms.TextBox();
            this.bRegistrarAfiliados = new System.Windows.Forms.Button();
            this.bUpdate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbCodigo
            // 
            this.tbCodigo.Location = new System.Drawing.Point(230, 63);
            this.tbCodigo.Name = "tbCodigo";
            this.tbCodigo.Size = new System.Drawing.Size(131, 22);
            this.tbCodigo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(114, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Codigo";
            // 
            // tbNombre
            // 
            this.tbNombre.Location = new System.Drawing.Point(230, 120);
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.Size = new System.Drawing.Size(131, 22);
            this.tbNombre.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(114, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Primer Nombre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(390, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Segundo Nombre";
            // 
            // tbSegundoNombre
            // 
            this.tbSegundoNombre.Location = new System.Drawing.Point(546, 120);
            this.tbSegundoNombre.Name = "tbSegundoNombre";
            this.tbSegundoNombre.Size = new System.Drawing.Size(131, 22);
            this.tbSegundoNombre.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(716, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Primer Apellido";
            // 
            // tbPrimerApellido
            // 
            this.tbPrimerApellido.Location = new System.Drawing.Point(843, 118);
            this.tbPrimerApellido.Name = "tbPrimerApellido";
            this.tbPrimerApellido.Size = new System.Drawing.Size(131, 22);
            this.tbPrimerApellido.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(999, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Segundo Apellido";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(1149, 120);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(131, 22);
            this.textBox1.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(114, 191);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Email Primario";
            // 
            // tbEmailPrimario
            // 
            this.tbEmailPrimario.Location = new System.Drawing.Point(230, 188);
            this.tbEmailPrimario.Name = "tbEmailPrimario";
            this.tbEmailPrimario.Size = new System.Drawing.Size(131, 22);
            this.tbEmailPrimario.TabIndex = 11;
            this.tbEmailPrimario.Text = " ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(390, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Email Secundario";
            // 
            // tbEmailSecundario
            // 
            this.tbEmailSecundario.Location = new System.Drawing.Point(546, 188);
            this.tbEmailSecundario.Name = "tbEmailSecundario";
            this.tbEmailSecundario.Size = new System.Drawing.Size(131, 22);
            this.tbEmailSecundario.TabIndex = 13;
            this.tbEmailSecundario.Text = " ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(88, 264);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 17);
            this.label8.TabIndex = 14;
            this.label8.Text = "Numero Prestamos";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // tbNumeroPrestamo
            // 
            this.tbNumeroPrestamo.Location = new System.Drawing.Point(230, 261);
            this.tbNumeroPrestamo.Name = "tbNumeroPrestamo";
            this.tbNumeroPrestamo.Size = new System.Drawing.Size(131, 22);
            this.tbNumeroPrestamo.TabIndex = 15;
            this.tbNumeroPrestamo.Text = " ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(390, 264);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 17);
            this.label9.TabIndex = 16;
            this.label9.Text = "Numero Cuenta";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // tbNumeroCuenta
            // 
            this.tbNumeroCuenta.Location = new System.Drawing.Point(546, 261);
            this.tbNumeroCuenta.Name = "tbNumeroCuenta";
            this.tbNumeroCuenta.Size = new System.Drawing.Size(131, 22);
            this.tbNumeroCuenta.TabIndex = 17;
            this.tbNumeroCuenta.Text = " ";
            // 
            // bRegistrarAfiliados
            // 
            this.bRegistrarAfiliados.Location = new System.Drawing.Point(382, 312);
            this.bRegistrarAfiliados.Name = "bRegistrarAfiliados";
            this.bRegistrarAfiliados.Size = new System.Drawing.Size(196, 39);
            this.bRegistrarAfiliados.TabIndex = 18;
            this.bRegistrarAfiliados.Text = "Registrar";
            this.bRegistrarAfiliados.UseVisualStyleBackColor = true;
            this.bRegistrarAfiliados.Click += new System.EventHandler(this.bRegistrarAfiliados_Click);
            // 
            // bUpdate
            // 
            this.bUpdate.Location = new System.Drawing.Point(674, 315);
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(179, 33);
            this.bUpdate.TabIndex = 19;
            this.bUpdate.Text = "Update";
            this.bUpdate.UseVisualStyleBackColor = true;
            // 
            // ManageAfiliados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1303, 376);
            this.Controls.Add(this.bUpdate);
            this.Controls.Add(this.bRegistrarAfiliados);
            this.Controls.Add(this.tbNumeroCuenta);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbNumeroPrestamo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbEmailSecundario);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbEmailPrimario);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbPrimerApellido);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbSegundoNombre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbNombre);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbCodigo);
            this.Name = "ManageAfiliados";
            this.Text = "ManageAfiliados";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbCodigo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbSegundoNombre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbPrimerApellido;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbEmailPrimario;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbEmailSecundario;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbNumeroPrestamo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbNumeroCuenta;
        private System.Windows.Forms.Button bRegistrarAfiliados;
        private System.Windows.Forms.Button bUpdate;
    }
}