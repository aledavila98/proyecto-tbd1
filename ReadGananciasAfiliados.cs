﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoTBD1
{
    public partial class ReadGananciasAfiliados : Form
    {
        public ReadGananciasAfiliados()
        {
            InitializeComponent();
        }

        private void gANANCIAS_AFILIADOSBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.gANANCIAS_AFILIADOSBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSetProyecto);

        }

        private void ReadGananciasAfiliados_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSetProyecto.GANANCIAS_AFILIADOS' table. You can move, or remove it, as needed.
            this.gANANCIAS_AFILIADOSTableAdapter.Fill(this.dataSetProyecto.GANANCIAS_AFILIADOS);

        }
    }
}
