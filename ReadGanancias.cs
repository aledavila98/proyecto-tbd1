﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoTBD1
{
    public partial class ReadGanancias : Form
    {
        public ReadGanancias()
        {
            InitializeComponent();
        }

        private void gANANCIASBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.gANANCIASBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSetProyecto);

        }

        private void ReadGanancias_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSetProyecto.GANANCIAS' table. You can move, or remove it, as needed.
            this.gANANCIASTableAdapter.Fill(this.dataSetProyecto.GANANCIAS);

        }
    }
}
