﻿namespace ProyectoTBD1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dataSetProyecto = new ProyectoTBD1.DataSetProyecto();
            this.tELEFONOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tELEFONOTableAdapter = new ProyectoTBD1.DataSetProyectoTableAdapters.TELEFONOTableAdapter();
            this.tableAdapterManager = new ProyectoTBD1.DataSetProyectoTableAdapters.TableAdapterManager();
            this.tELEFONOBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tELEFONOBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bReadAfiliados = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.bReadAbonos = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.bReadCuentas = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.bReadGanancias = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.bReadGananciasAfiliados = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.bReadPrestamos = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.bReadPagos = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.bReadTelefonos = new System.Windows.Forms.Button();
            this.bReporteDividendos = new System.Windows.Forms.Button();
            this.bManageAfiliados = new System.Windows.Forms.Button();
            this.bManageAbonos = new System.Windows.Forms.Button();
            this.bManageCuentas = new System.Windows.Forms.Button();
            this.bManageGanancias = new System.Windows.Forms.Button();
            this.bManageGananciasAfiliadosbManageGananciasAfiliados = new System.Windows.Forms.Button();
            this.bManagePrestamos = new System.Windows.Forms.Button();
            this.bManagePagos = new System.Windows.Forms.Button();
            this.bManageTelefonos = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProyecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tELEFONOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tELEFONOBindingNavigator)).BeginInit();
            this.tELEFONOBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataSetProyecto
            // 
            this.dataSetProyecto.DataSetName = "DataSetProyecto";
            this.dataSetProyecto.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tELEFONOBindingSource
            // 
            this.tELEFONOBindingSource.DataMember = "TELEFONO";
            this.tELEFONOBindingSource.DataSource = this.dataSetProyecto;
            // 
            // tELEFONOTableAdapter
            // 
            this.tELEFONOTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ABONOSTableAdapter = null;
            this.tableAdapterManager.AFILIADOSTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CUENTASTableAdapter = null;
            this.tableAdapterManager.GANANCIAS_AFILIADOSTableAdapter = null;
            this.tableAdapterManager.GANANCIASTableAdapter = null;
            this.tableAdapterManager.PAGOSTableAdapter = null;
            this.tableAdapterManager.PRESTAMOSTableAdapter = null;
            this.tableAdapterManager.TELEFONOTableAdapter = this.tELEFONOTableAdapter;
            this.tableAdapterManager.UpdateOrder = ProyectoTBD1.DataSetProyectoTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // tELEFONOBindingNavigator
            // 
            this.tELEFONOBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.tELEFONOBindingNavigator.BindingSource = this.tELEFONOBindingSource;
            this.tELEFONOBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.tELEFONOBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.tELEFONOBindingNavigator.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.tELEFONOBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.tELEFONOBindingNavigatorSaveItem});
            this.tELEFONOBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.tELEFONOBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.tELEFONOBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.tELEFONOBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.tELEFONOBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.tELEFONOBindingNavigator.Name = "tELEFONOBindingNavigator";
            this.tELEFONOBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.tELEFONOBindingNavigator.Size = new System.Drawing.Size(969, 27);
            this.tELEFONOBindingNavigator.TabIndex = 0;
            this.tELEFONOBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(45, 24);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 27);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(24, 24);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // tELEFONOBindingNavigatorSaveItem
            // 
            this.tELEFONOBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tELEFONOBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("tELEFONOBindingNavigatorSaveItem.Image")));
            this.tELEFONOBindingNavigatorSaveItem.Name = "tELEFONOBindingNavigatorSaveItem";
            this.tELEFONOBindingNavigatorSaveItem.Size = new System.Drawing.Size(24, 24);
            this.tELEFONOBindingNavigatorSaveItem.Text = "Save Data";
            this.tELEFONOBindingNavigatorSaveItem.Click += new System.EventHandler(this.tELEFONOBindingNavigatorSaveItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(70, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 29);
            this.label2.TabIndex = 5;
            this.label2.Text = "Afiliados";
            // 
            // bReadAfiliados
            // 
            this.bReadAfiliados.Location = new System.Drawing.Point(694, 130);
            this.bReadAfiliados.Name = "bReadAfiliados";
            this.bReadAfiliados.Size = new System.Drawing.Size(129, 33);
            this.bReadAfiliados.TabIndex = 6;
            this.bReadAfiliados.Text = "Read";
            this.bReadAfiliados.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(70, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 29);
            this.label3.TabIndex = 7;
            this.label3.Text = "Abonos";
            // 
            // bReadAbonos
            // 
            this.bReadAbonos.Location = new System.Drawing.Point(694, 205);
            this.bReadAbonos.Name = "bReadAbonos";
            this.bReadAbonos.Size = new System.Drawing.Size(129, 33);
            this.bReadAbonos.TabIndex = 11;
            this.bReadAbonos.Text = "Read";
            this.bReadAbonos.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(70, 283);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 29);
            this.label4.TabIndex = 12;
            this.label4.Text = "Cuentas";
            // 
            // bReadCuentas
            // 
            this.bReadCuentas.Location = new System.Drawing.Point(694, 282);
            this.bReadCuentas.Name = "bReadCuentas";
            this.bReadCuentas.Size = new System.Drawing.Size(129, 33);
            this.bReadCuentas.TabIndex = 16;
            this.bReadCuentas.Text = "Read";
            this.bReadCuentas.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(70, 355);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 29);
            this.label5.TabIndex = 17;
            this.label5.Text = "Ganancias";
            // 
            // bReadGanancias
            // 
            this.bReadGanancias.Location = new System.Drawing.Point(694, 355);
            this.bReadGanancias.Name = "bReadGanancias";
            this.bReadGanancias.Size = new System.Drawing.Size(129, 33);
            this.bReadGanancias.TabIndex = 21;
            this.bReadGanancias.Text = "Read";
            this.bReadGanancias.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(41, 430);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(224, 29);
            this.label6.TabIndex = 22;
            this.label6.Text = "Ganancias Afiliados";
            // 
            // bReadGananciasAfiliados
            // 
            this.bReadGananciasAfiliados.Location = new System.Drawing.Point(694, 426);
            this.bReadGananciasAfiliados.Name = "bReadGananciasAfiliados";
            this.bReadGananciasAfiliados.Size = new System.Drawing.Size(129, 33);
            this.bReadGananciasAfiliados.TabIndex = 26;
            this.bReadGananciasAfiliados.Text = "Read";
            this.bReadGananciasAfiliados.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(70, 498);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 29);
            this.label7.TabIndex = 27;
            this.label7.Text = "Prestamos";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // bReadPrestamos
            // 
            this.bReadPrestamos.Location = new System.Drawing.Point(694, 500);
            this.bReadPrestamos.Name = "bReadPrestamos";
            this.bReadPrestamos.Size = new System.Drawing.Size(129, 33);
            this.bReadPrestamos.TabIndex = 31;
            this.bReadPrestamos.Text = "Read";
            this.bReadPrestamos.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(70, 579);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 29);
            this.label8.TabIndex = 32;
            this.label8.Text = "Pagos";
            // 
            // bReadPagos
            // 
            this.bReadPagos.Location = new System.Drawing.Point(694, 581);
            this.bReadPagos.Name = "bReadPagos";
            this.bReadPagos.Size = new System.Drawing.Size(129, 33);
            this.bReadPagos.TabIndex = 36;
            this.bReadPagos.Text = "Read";
            this.bReadPagos.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(70, 668);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 29);
            this.label9.TabIndex = 37;
            this.label9.Text = "Telefonos";
            // 
            // bReadTelefonos
            // 
            this.bReadTelefonos.Location = new System.Drawing.Point(694, 670);
            this.bReadTelefonos.Name = "bReadTelefonos";
            this.bReadTelefonos.Size = new System.Drawing.Size(129, 33);
            this.bReadTelefonos.TabIndex = 41;
            this.bReadTelefonos.Text = "Read";
            this.bReadTelefonos.UseVisualStyleBackColor = true;
            // 
            // bReporteDividendos
            // 
            this.bReporteDividendos.Location = new System.Drawing.Point(399, 738);
            this.bReporteDividendos.Name = "bReporteDividendos";
            this.bReporteDividendos.Size = new System.Drawing.Size(153, 23);
            this.bReporteDividendos.TabIndex = 42;
            this.bReporteDividendos.Text = "Reporte Dividendos";
            this.bReporteDividendos.UseVisualStyleBackColor = true;
            // 
            // bManageAfiliados
            // 
            this.bManageAfiliados.Location = new System.Drawing.Point(387, 132);
            this.bManageAfiliados.Name = "bManageAfiliados";
            this.bManageAfiliados.Size = new System.Drawing.Size(165, 28);
            this.bManageAfiliados.TabIndex = 43;
            this.bManageAfiliados.Text = "Manage";
            this.bManageAfiliados.UseVisualStyleBackColor = true;
            // 
            // bManageAbonos
            // 
            this.bManageAbonos.Location = new System.Drawing.Point(387, 205);
            this.bManageAbonos.Name = "bManageAbonos";
            this.bManageAbonos.Size = new System.Drawing.Size(165, 28);
            this.bManageAbonos.TabIndex = 44;
            this.bManageAbonos.Text = "Manage";
            this.bManageAbonos.UseVisualStyleBackColor = true;
            // 
            // bManageCuentas
            // 
            this.bManageCuentas.Location = new System.Drawing.Point(387, 287);
            this.bManageCuentas.Name = "bManageCuentas";
            this.bManageCuentas.Size = new System.Drawing.Size(165, 28);
            this.bManageCuentas.TabIndex = 45;
            this.bManageCuentas.Text = "Manage";
            this.bManageCuentas.UseVisualStyleBackColor = true;
            // 
            // bManageGanancias
            // 
            this.bManageGanancias.Location = new System.Drawing.Point(387, 360);
            this.bManageGanancias.Name = "bManageGanancias";
            this.bManageGanancias.Size = new System.Drawing.Size(165, 28);
            this.bManageGanancias.TabIndex = 46;
            this.bManageGanancias.Text = "Manage";
            this.bManageGanancias.UseVisualStyleBackColor = true;
            // 
            // bManageGananciasAfiliadosbManageGananciasAfiliados
            // 
            this.bManageGananciasAfiliadosbManageGananciasAfiliados.Location = new System.Drawing.Point(387, 428);
            this.bManageGananciasAfiliadosbManageGananciasAfiliados.Name = "bManageGananciasAfiliadosbManageGananciasAfiliados";
            this.bManageGananciasAfiliadosbManageGananciasAfiliados.Size = new System.Drawing.Size(165, 28);
            this.bManageGananciasAfiliadosbManageGananciasAfiliados.TabIndex = 47;
            this.bManageGananciasAfiliadosbManageGananciasAfiliados.Text = "Manage";
            this.bManageGananciasAfiliadosbManageGananciasAfiliados.UseVisualStyleBackColor = true;
            // 
            // bManagePrestamos
            // 
            this.bManagePrestamos.Location = new System.Drawing.Point(387, 498);
            this.bManagePrestamos.Name = "bManagePrestamos";
            this.bManagePrestamos.Size = new System.Drawing.Size(165, 28);
            this.bManagePrestamos.TabIndex = 48;
            this.bManagePrestamos.Text = "Manage";
            this.bManagePrestamos.UseVisualStyleBackColor = true;
            // 
            // bManagePagos
            // 
            this.bManagePagos.Location = new System.Drawing.Point(387, 580);
            this.bManagePagos.Name = "bManagePagos";
            this.bManagePagos.Size = new System.Drawing.Size(165, 28);
            this.bManagePagos.TabIndex = 49;
            this.bManagePagos.Text = "Manage";
            this.bManagePagos.UseVisualStyleBackColor = true;
            // 
            // bManageTelefonos
            // 
            this.bManageTelefonos.Location = new System.Drawing.Point(387, 668);
            this.bManageTelefonos.Name = "bManageTelefonos";
            this.bManageTelefonos.Size = new System.Drawing.Size(165, 28);
            this.bManageTelefonos.TabIndex = 50;
            this.bManageTelefonos.Text = "Manage";
            this.bManageTelefonos.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 783);
            this.Controls.Add(this.bManageTelefonos);
            this.Controls.Add(this.bManagePagos);
            this.Controls.Add(this.bManagePrestamos);
            this.Controls.Add(this.bManageGananciasAfiliadosbManageGananciasAfiliados);
            this.Controls.Add(this.bManageGanancias);
            this.Controls.Add(this.bManageCuentas);
            this.Controls.Add(this.bManageAbonos);
            this.Controls.Add(this.bManageAfiliados);
            this.Controls.Add(this.bReporteDividendos);
            this.Controls.Add(this.bReadTelefonos);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.bReadPagos);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.bReadPrestamos);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.bReadGananciasAfiliados);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bReadGanancias);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bReadCuentas);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.bReadAbonos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bReadAfiliados);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tELEFONOBindingNavigator);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProyecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tELEFONOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tELEFONOBindingNavigator)).EndInit();
            this.tELEFONOBindingNavigator.ResumeLayout(false);
            this.tELEFONOBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSetProyecto dataSetProyecto;
        private System.Windows.Forms.BindingSource tELEFONOBindingSource;
        private DataSetProyectoTableAdapters.TELEFONOTableAdapter tELEFONOTableAdapter;
        private DataSetProyectoTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator tELEFONOBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton tELEFONOBindingNavigatorSaveItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bReadAfiliados;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bReadAbonos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button bReadCuentas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bReadGanancias;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bReadGananciasAfiliados;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button bReadPrestamos;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button bReadPagos;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button bReadTelefonos;
        private System.Windows.Forms.Button bReporteDividendos;
        private System.Windows.Forms.Button bManageAfiliados;
        private System.Windows.Forms.Button bManageAbonos;
        private System.Windows.Forms.Button bManageCuentas;
        private System.Windows.Forms.Button bManageGanancias;
        private System.Windows.Forms.Button bManageGananciasAfiliadosbManageGananciasAfiliados;
        private System.Windows.Forms.Button bManagePrestamos;
        private System.Windows.Forms.Button bManagePagos;
        private System.Windows.Forms.Button bManageTelefonos;
    }
}

